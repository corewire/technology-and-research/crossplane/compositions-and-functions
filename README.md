# Argocd Demo

## Preparation (10 - 15 minutes)

### dependencies
- docker
- kind
- kubectl
- jq
- envsubst
- base64
- sed

### Setup demo environment:

```bash
$ bash setup_demo.sh
```

The script will information similar to:
```
#################################################
You're ready to go!
ArgoCD is ready at https://172.16.80.30:443
ArgoCD login: admin / F2Dc6HCUTEBdq920
-------------------------------------------------
Keycloak is ready at http://172.16.80.31:80/auth
Keycloak login: admin / admin
#################################################
```

### Apply demos

Apply the demos in ./demo with

```
$ kubectl apply -f demo/01-plain-user
$ kubectl apply -f demo/02-plain-composition/01<TAB>
...
```

See the effects by login into the keycloak admin console.

### Custom function

See `setup_functions.sh` for instructions on how to bundle the function.
