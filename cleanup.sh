#!/bin/bash
set -eo pipefail

CLUSTER_NAME=$1

if [[ -n $CLUSTER_NAME ]]; then
  echo "Cluster name provided: $CLUSTER_NAME"
else
  echo "Default cluster name: fenrir-1"
  CLUSTER_NAME="fenrir-1"
fi

echo "########### Checking dependencies ###########"
command -v docker >/dev/null 2>&1 || { echo >&2 "Docker is required but not installed.  Aborting."; exit 1; }
command -v kind >/dev/null 2>&1 || { echo >&2 "Kind is required but not installed.  Aborting."; exit 1; }
echo "All dependencies are installed."

# check if user can run docker without sudo, if not create an alias for sudo docker for this session
if ! docker ps >/dev/null 2>&1; then
  echo "Sudo required for docker."
  sudo_prefix='sudo'
else
  sudo_prefix=''
fi

echo "########### Stop cluster ###########"
$sudo_prefix kind delete cluster --name $CLUSTER_NAME
