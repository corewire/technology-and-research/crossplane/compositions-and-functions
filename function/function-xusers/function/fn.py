"""A Crossplane composition function."""

import grpc
from crossplane.function import logging, response
from crossplane.function.proto.v1beta1 import run_function_pb2 as fnv1beta1
from crossplane.function.proto.v1beta1 import run_function_pb2_grpc as grpcv1beta1


class FunctionRunner(grpcv1beta1.FunctionRunnerService):
    """A FunctionRunner handles gRPC RunFunctionRequests."""

    def __init__(self):
        """Create a new FunctionRunner."""
        self.log = logging.get_logger()

    async def RunFunction(
        self, req: fnv1beta1.RunFunctionRequest, _: grpc.aio.ServicerContext
    ) -> fnv1beta1.RunFunctionResponse:
        """Run the function."""
        log = self.log.bind(tag=req.meta.tag)

        rsp = response.to(req)

        realm = req.observed.composite.resource["spec"]["realm"]
        prefix = req.observed.composite.resource["spec"]["prefix"]
        count = int(req.input["count"])

        log.info(
            "I was run with the following parameters",
            count=count,
            realm=realm,
            prefix=prefix,
        )

        for i in range(count):
            log.info(f"Updating resource {prefix}-{i}")
            rsp.desired.resources[f"{prefix}-{i}"].resource.update({
                    "apiVersion": "user.keycloak.crossplane.io/v1alpha1",
                    "kind": "User",
                    "metadata": {"name": f"{prefix}-user-{i}"},
                    "spec": {
                        "forProvider": {
                            "realmId": realm,
                            "username": f"{prefix}-user-{i}",
                        },
                        "providerConfigRef": {"name": "keycloak-config"},
                    },
                })

        return rsp
