#!/bin/bash
set -eo pipefail

IMAGE_TAG='v0.1.4'

echo "########### Checking dependencies ###########"
command -v docker >/dev/null 2>&1 || { echo >&2 "Docker is required but not installed.  Aborting."; exit 1; }
command -v crossplane >/dev/null 2>&1 || { echo >&2 "crossplane-cli is required but not installed.  Aborting."; exit 1; }
echo "All dependencies are installed."

# check if user can run docker without sudo, if not create an alias for sudo docker for this session
if ! docker ps >/dev/null 2>&1; then
  echo "Sudo required for docker."
  sudo_prefix='sudo'
else
  sudo_prefix=''
fi

echo "########### Build xpkg ###########"
find "./function" -mindepth 1 -maxdepth 1 -type d | while read folder; do
    echo "###### Processing folder: $folder"
    pushd "$folder"

    folder_name=$(basename "$folder")
    runtime=${folder_name}-runtime-amd64
    echo "###### Building runtime: ${runtime}"
    $sudo_prefix docker build . --platform=linux/amd64 --tag ${runtime}

    echo "###### Building package"
    $sudo_prefix crossplane xpkg build --package-root=package --embed-runtime-image=${runtime} --package-file=function-amd64.xpkg

    echo "###### Load package into local docker cache"
    output=$($sudo_prefix docker load -i function-amd64.xpkg)
    sha_value=$(echo "$output" | grep -oP 'sha256:\K[a-f0-9]{64}')
    echo $output
    echo "SHA value: $sha_value"

    echo "###### Tagging image"
    $sudo_prefix docker tag $sha_value registry.gitlab.com/corewire/technology-and-research/crossplane/compositions-and-functions/$folder_name:${IMAGE_TAG}

    echo "###### Pushing image"
    $sudo_prefix docker push registry.gitlab.com/corewire/technology-and-research/crossplane/compositions-and-functions/$folder_name:${IMAGE_TAG}

    popd
done
